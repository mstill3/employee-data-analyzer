// Plots the table on ChartJS
function plot(tabledata) 
{

    // shows canvas on the html page
	var html = '<canvas id="line-chart" width="500px;" height="80px"></canvas>';
	document.getElementById("mygraph").innerHTML = html; 

 
    // percentile interval on x axis
	var labels = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120]; 

	// initialize data count array
	var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; 

    // for each percentile group sets the number of counts for the y axis
	Object.keys(tabledata).forEach(function(key) {
		var group = parseInt(key);
		data[group / 10] = tabledata[key];
	});

    // sets the color to be white
	Chart.defaults.global.defaultFontColor='rgb(220, 220, 220)';	

    // creates new chart instance with the data values plotted
	new Chart(document.getElementById("line-chart"), {
	  	type: 'line',
	  	data: {
	  	  	labels: labels,
	  	  	datasets: [{ 
	  	  	    	data: data,
	  	  	    	label: "Utilization",
	  	  	    	borderColor: "#3e95cd",
	  	  	    	backgroundColor: "#7cb7dd",
	  	  	    	fill: true
	  	  	}]
	  	},
	  	options: {
	  	  	title: {
	  	  		display: true,
	  	  		text: 'Utilization Counts'
	  	  	}
	  	}
	});

}
