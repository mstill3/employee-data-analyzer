// Uses JQuery to select specific dates
$(function() 
{

    // Sets up the date range picker
	$('input[name="dates"]').daterangepicker(
    {
        // Open date range picker on the left hand side
    	opens: 'left'
  	}, function(start, end, label) 
    {
        // Loading icon
		var html = '<i id="cog" class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>';
		document.getElementById("loading").innerHTML = html; 

        // Pull start and end dates and start angularjs method for processing
    	start = start.format('YYYY-MM-DD');
        end = end.format('YYYY-MM-DD');
        angular.element(document.getElementById('main')).scope().process(start, end);
  	});

});

