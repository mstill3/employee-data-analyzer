app.controller('MainController', ['$scope', '$http', function ($scope, $http) 
{

    // Initilizes the table by pulling the total number of employees
    $scope.init = function()
    {
        // Initializes the scope variable
        $scope.results = [];
        $scope.orderByField = 'code';
        $scope.reverseSort = false;
        $scope.tabledatalength = 0;
        $scope.numemployees = '0';
        $scope.showing = "Showing " + $scope.tabledatalength + " out of " + $scope.numemployees;

        // Gets the num employees from the backend
        var url = "/numemployees";
        var data = new FormData();

        // Set the configurations for the uploaded file
        var config =
        {
            transformRequest: angular.identity,
            transformResponse: angular.identity,
            headers: 
            {
                'Content-Type': undefined
            }
        }

        // Hits endpoint to grab data
        $http.get(url, data, config).then(function (response)
        {
            $scope.numemployees = response.data;
            $scope.showing = "Showing " + $scope.tabledatalength + " out of " + $scope.numemployees;
        });

    }

    // Sends the dates selected off to backend to perform spark processing and returned to table and graph
    $scope.process = function(start, end)
    {   
        // Sends dates to backend endpoint to run spark processing
        var url = "/compute?dates=" + start + ":" + end;
        var data = new FormData();

        // Set the configurations for the uploaded file
        var config =
        {
            transformRequest: angular.identity,
            transformResponse: angular.identity,
            headers: 
            {
                'Content-Type': undefined
            }
        }

        // Sends the file data off to retrieve results
        $http.get(url, data, config).then(function (response) 
        {
            // Sets the scope variables to the spark results
            $scope.tabledata = response.data.table;
            $scope.tabledatalength = $scope.tabledata.length;
            $scope.graphdata = response.data.graph;

            $scope.showing = "Showing " + $scope.tabledatalength + " out of " + $scope.numemployees;

            // Plots the graph data, and remove loading icon
            plot(response.data.graph);
            document.getElementById("loading").innerHTML = ""; 
        });

    }

    // Called to save current table off to a csv file
    $scope.download = function()
    {
        var url = "/download";
        var data = new FormData();

        // Set the configurations for the uploaded file
        var config =
        {
            transformRequest: angular.identity,
            transformResponse: angular.identity,
            headers: 
            {
                'Content-Type': undefined
            }
        }

        // Sends the file data off to be saved
        $http.get(url, data, config).then(function (response) 
        {
            $scope.saved = response.data.saved;
        });

    }

}]);
