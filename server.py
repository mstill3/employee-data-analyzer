#!/usr/bin/env python3

# Flask web server
# Written by: Matt Stillwell

from flask import Flask
from flask import render_template
from flask import request 
import os
import sys

# Finds absolute file path so it does not matter where the file is called from
dir_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(dir_path + "src/")
from sparkjob import DataAnalyzer 

# Set the project root directory as the static folder, you can set others.
app = Flask(__name__)
PORT = 8080


# Endpoints
@app.route("/")
def index():
    return render_template('index.html')


@app.route('/numemployees', methods = ['GET'])
def numemployees():
    return analyzer.get_num_employees()


@app.route('/compute', methods = ['GET'])
def spark():
    
    result = request.args.to_dict()
    dates = result['dates']

    #ie) 10/09/2019-12/05/2019
    start_date = dates[:dates.find(':')]
    end_date = dates[dates.find(':') + 1:]

    results = analyzer.run(start_date, end_date)
    return results 


@app.route('/download', methods = ['GET'])
def download():
    return {'saved': analyzer.download()}


# Auto run main method
if __name__ == "__main__":
    analyzer = DataAnalyzer()
    app.run(port=PORT)

