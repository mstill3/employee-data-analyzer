#!/usr/bin/env python3

# Spark job for analyzing work utilization
# Matt Stillwell

import os
import sys
import locale

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, when, to_date, regexp_replace, lit, format_number, round, broadcast
from pyspark.sql.types import IntegerType
from datetime import datetime

# Finds absolute file path so it does not matter where the file is called from
dir_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(dir_path)


class DataAnalyzer(object):
    ''' Analyzing work utilization of employees over a given time frame '''

    def __init__(self, debug=False, calendar_file='calendar.csv', employee_file='employees.csv'):
        ''' Constructor for reading in the calender and employee data '''

        locale.setlocale(locale.LC_ALL, '')
        calendar_path = dir_path + 'data/' + calendar_file
        employee_path = dir_path + 'data/' + employee_file
        self.debug = debug
        self.HOURS_PER_DAY = 8
        self.saveableDF = None
        
        self.spark = SparkSession.builder.appName('utilization').getOrCreate()
        
        #===================================
        #  CALENDAR
        #===================================
        self.calenderDF = (
        
            self.spark.read
                      .option('header', 'true')
                      .option('inferSchema', 'true')
                      .csv(calendar_path)
                      .withColumn('date', to_date( col("Date"), "MM/dd/yyyy"))
                      .withColumn('billable', when(col('Work Days') == 0, False).otherwise(True))
                      .orderBy('date')
                      .select('date', 'billable')
                      .cache()
        
        )
        
        print("\n===============\nCALENDAR DF\n===============")
        self.calenderDF.printSchema()
        self.calenderDF.show()
        
        #===================================
        #  EMPLOYEE HOURS
        #===================================
        self.employeeHoursDF = (
        
            self.spark.read
                      .option('header', 'true')
                      .option('inferSchema', 'true')
                      .csv(employee_path)
                      .withColumn('firstName', regexp_replace('Name', r'.*, (\S*).*', '$1'))
                      .withColumn('lastName', regexp_replace('Name', r'(,.*)', ''))
                      .withColumn('date', to_date('Work_Date', "MM/dd/yyyy"))
                      .withColumnRenamed('Code', 'code')
                      .withColumnRenamed('Account_Desc', 'description')
                      .withColumnRenamed('Hours_Worked', 'hours')
                      .orderBy('date')
                      .select('date', 'code', 'lastName', 'firstName', 'description', 'hours')
                      .cache()
        
        )
        
        print("\n===============\nEMPLOYEE HOURS DF\n===============")
        self.employeeHoursDF.printSchema()
        self.employeeHoursDF.show()
        

        #===================================
        #  EMPLOYEE INFO
        #===================================
        # get single employee rows with startDate
        self.employeeInfoDF = (

            self.employeeHoursDF.orderBy(col('date'))
                           .dropDuplicates(['code']) # dropping duplicates when sorrted by date will be startdate
                           .select('code', 'lastName', 'firstName', col('date').alias('startDate'))  
        )
        
        # get single employee rows with endDate
        endDF = (
            
            self.employeeHoursDF.orderBy(col('date').desc())
                           .dropDuplicates(['code'])  # dropping duplicates when sorrted back by date will be enddate
                           .select('code', col('date').alias('endDate'))
        
        )
        
        # join together to get single employee rows with startDate and endDate
        self.employeeInfoDF = (
        
            endDF.join(broadcast(self.employeeInfoDF), 'code')
                          .select('code', 'startDate', 'endDate')
                          .sort('code')
        
        )
        

        # employeeInfoDF joined to entire employeeHoursDF
        self.employeeHoursPlusDF= (
        
            self.employeeHoursDF.join(broadcast(self.employeeInfoDF), 'code')
                                .sort('code')
        
        )
        
        #if(self.debug):
        print("\n===============\nEMPLOYEE INFO DF\n===============")
        self.employeeInfoDF.printSchema()
        self.employeeInfoDF.show()
        print("\n===============\nEMPLOYEE HOURS PLUS DF\n===============")
        self.employeeHoursPlusDF.printSchema()
        self.employeeHoursPlusDF.show()

        
        
        
    def get_num_employees(self):
        ''' Returns the number of employees from the data '''
        # int num employees with , seperator
        return locale.format("%d", self.employeeInfoDF.count(), grouping=True)
    
    
    def run(self, selected_start_date_str, selected_end_date_str):
        ''' Caluclates the individual employee time utilization and returns the results '''

        #==========================
        #  PROGRAM
        #==========================

        # convert input date strings to datetime objects
        selected_start_date = datetime.strptime(selected_start_date_str, '%Y-%m-%d').date()
        selected_end_date = datetime.strptime(selected_end_date_str, '%Y-%m-%d').date()
        
        
        # count number of billable days in that range
        rangedCalendarDF = (
            
            self.calenderDF.filter(col('date') >= selected_start_date)
                      .filter(col('date') <= selected_end_date)
                      .filter(col('billable') == True)
                      .orderBy(col('date'))
        
        )
        
        
        
        # calculate total potential hours from those days
        num_days = rangedCalendarDF.count()
        potential_hours = self.HOURS_PER_DAY * num_days
        
        # dict to hold code and potential hours
        available_hours = {}
        lis = self.employeeInfoDF.collect()
        for employee in lis:
                
                # Pull each id, start and stop date per employee
                eid = employee['code']
                start_date = employee['startDate']
                end_date = employee['endDate']
                
                # If the selected dates are a lesser range change the values for calculations
                if selected_start_date > start_date:
                    start_date = selected_start_date
                
                if selected_end_date < end_date:
                    end_date = selected_end_date
                
                
                # create calender for that interns specific start and stop date
                num_employee_billable_days = (self.calenderDF.filter(col('date') >= start_date)
                                                .filter(col('date') <= end_date)
                                                .filter(col('billable') == True)
                                                .count()
                                             )
                available_hours[eid] = num_employee_billable_days * 8
        
        

        
        # get ranged sum dataframe of the employees hours and append on more cols
        allDF = (
            
            self.employeeHoursPlusDF.filter(col('date') >= selected_start_date)
                      .filter(col('date') <= selected_end_date)
                      .groupBy('code', 'lastName', 'firstName', 'startDate', 'endDate')
                      .sum('hours')
                      .withColumnRenamed('sum(hours)', 'worked')
                      .withColumn('available', col('code'))
        
        )

        if allDF.count() == 0 :
            return {'table': [], 'graph': {}}
        

        allDF = allDF.rdd.map(lambda row: (row[0], row[1], row[2], row[3], row[4], row[5], available_hours[row[6]])).toDF(['code', 'lastName', 'firstName', 'startDate', 'endDate', 'worked', 'available'])
        allDF = allDF.withColumn('utilization', format_number( (col('worked') / col('available') * 100), 2))

        # allDF.show()
        
        #===================
        #  ELSE
        #===================

        # Make DF saveable              
        self.basename = 'downloads/' + selected_start_date_str + '-' + selected_end_date_str + '#'
        self.saveableDF = allDF
        

        # sends df to json format
        tabledata = []
        rows = allDF.collect()
        
        for row in rows:
            tabledata.append({
                    'code': str(row['code']).zfill(6),
                    'lastName': row['lastName'],
                    'firstName': row['firstName'],
                    'startDate': row['startDate'].strftime("%Y-%m-%d"),
                    'endDate': row['endDate'].strftime("%Y-%m-%d"),
                    'worked': float(row['worked']),
                    'available': float(row['available']),
                    'utilization': float(row['utilization'])
                })



        # BUILD GRAPH
        allRoundDF = allDF.withColumn('utilizationRounded', round('utilization', -1))
        allRoundDF = allRoundDF.groupBy('utilizationRounded').count()


        # DEBUG PRINT           
        if(self.debug):
                print("\n===============\nAVAILABLE HOURS DICT\n===============")
                print(available_hours)
                print("\n===============\nALL DF\n===============")
                allDF.printSchema()
                allDF.show()
                print("\n===============\nALL ROUND DF\n===============")
                allRoundDF.printSchema()
                allRoundDF.show()

        
        # sends df to json format
        graphdata = {}
        rows = allRoundDF.collect()
        
        for row in rows:
            graphdata[row['utilizationRounded']] = row['count']

            
        results = {'table' : tabledata, 'graph': graphdata}
        return(results)
            
    

    def download(self):
        ''' Downloads the current DF off as a CSV file '''

        if(self.saveableDF != None):
            count = 1
            usedname = True

            while(usedname):
                filename = self.basename + str(count) + '.csv'  
                if(not os.path.exists(filename)):
                    usedname = False
                else:
                    count = count + 1
                                    
                    
            (self.saveableDF.repartition(1)
                            .write
                            .format("com.databricks.spark.csv")
                            .option("header", "true")
                            .save(filename)
            )

            self.saveableDF = None   # cant be saved again and cleans cache
            return('true')

        return('false')
