# Employee Data Analyzer

## Launch

To startup the project run:
`jupyter notebook`

Put this in the /opt/spark/spark-current/conf/spark-env.sh file:
```bash
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-amd64"
```
